$(document).ready(function() {
	var i = 0;
	
	$("div.out").mouseover(function() {
		$("p:first", this).text("mouse over");
		$("p:last", this).text(++i);
	}).mouseout(function() {
		$("p:first").text("mouse out");	
	});

	function maxopen(e) {
		var maxwindow = window.open(e.data.url, "", e.data.winAttributes);
		maxwindow.moveTo(0, 0);
		maxwindow.resizeTo(screen.availWidth, screen.availHeight);
	}

	$("#max_screen").on("click", {
		url: "http://www.google.com", 
		winAttributes: "resize=1, scrollbars=1, status=1"
	}, maxopen);

	// flash
	function flash() {
		$("#off_test").show().fadeOut("slow");
	}

	$("#bind").click(function() {
		$("body")
			.on("click", "#theone", flash)
			.find("#theone")
			.text("Can Click");
	})

	$("#unbind").click(function() {
		$("body")
			.off("click", "#theone", flash)
			.find("#theone")
			.text("Nothing");
	})

	// schedule
	$("#add_img").on("click", function() {
		$("#note_form").fadeIn();
	});

	$("#add_note").on("click", function() {
		var title = $("#note_title").val();
		var date = $("#note_date").val();
		var content = $("#note_content").val();

		$("#note").append(
			"<p>" + title + "<br>" + date + "<br>" + content + "</p>"
		)

		$("#note_form").slideUp().fadeOut("slow");

		$("#note_title").val("");
		$("#note_date").val("");
		$("#note_content").val("");
	});

	$("#moving_button").on("click", function() {
		$("#moving_box").animate({
			right: "0px",
			height: "+=50px",
			width: "+=50px"
		});

		$("#animation_test").animate({
			height: "+=50px"
		})
	});


	$(".accordion").each(function() {
		var dl = $(this);
		var alldd = dl.find("dd");
		var alldt = dl.find("dt");
		closeAll();

		function closeAll() {
			alldd.addClass("closed");
			alldt.addClass("closed");
		}

		function open(dt, dd) {
			dt.removeClass("closed");
			dd.removeClass("closed");
		}

		alldt.click(function() {
			closeAll();
			open($(this), $(this).next("dd"));	
		})
	});

	var interval = 3000;
	$(".slideshow").each(function() {
		var container = $(this);
		var timer;

		function switchImg() {
			var imgs = container.find("img");
			var first = imgs.eq(0);

			first.fadeOut();
			imgs.eq(1).fadeIn();
			container.append(first);
		}

		function startTimer() {
			timer = setInterval(switchImg, interval);			
		}

		function stopTimer() {
			clearInterval(timer);
		}

		startTimer();

		container.find("img").hover(stopTimer, startTimer);
	});

	$("#getText").on("click", function() {
		var url = (location.host) ? "data.txt" : "https://s3.ap-northeast-2.amazonaws.com/univweb/data.txt"
		var tb = $("<table/>");
		var row = $("<tr/>").append(
			$("<th/>").text("이름"),
			$("<th/>").text("아이디"),
			$("<th/>").text("학과"),
			$("<th/>").text("수강과목"),
		);
		tb.append(row);

		var req = $.ajax({
			url: url,
			dataType: "json"
		})

		req.done(function(data, status) {
			for (var i=0; i < data.length; i++) {
				row = $("<tr/>").append(
					$("<td/>").text(data[i].name),
					$("<td/>").text(data[i].id),
					$("<td/>").text(data[i].department),
					$("<td/>").text(data[i].class.toString())
				)
				tb.append(row);
			}
		})
		$("#tablebox").append(tb);
	})
})
