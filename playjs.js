// global variables
var computerNumber = Math.floor(Math.random() * 100 + 1);
const MONTHS = [
	"Jan", "Feb", "March", "April", "May", "Jun",
	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];

function calc() {
	var x = document.getElementById("x").value;
	var y = document.getElementById("y").value;
	var outputElem = document.getElementById("sum");

	outputElem.value = parseInt(x) + parseInt(y);
	return;
}

function replay() {
	var guessesElem = document.getElementById("guesses");
	computerNumber = Math.floor(Math.random() * 100 + 1);
	guessesElem.value = 0;
	return;
}

function guess() {
	var guessesElem = document.getElementById("guesses");
	var numElem = document.getElementById("user");
	var hintElem = document.getElementById("result");
	var num;

	guessesElem.value = parseInt(guessesElem.value) + 1;
	num = parseInt(numElem.value);

	if (num > computerNumber) {
		hintElem.value = "더 작습니다.";
	} else if (num < computerNumber) {
		hintElem.value = "더 큽니다.";
	} else {
		hintElem.value = "정답입니다.";
	}
	return;
}

function setCTime() {
	var timeElem = document.getElementById("ctime");
	var date = new Date();
	var seconds = date.getSeconds();
	if (seconds < 10) seconds = '0' + seconds;

	timeElem.innerHTML = (
		MONTHS[date.getMonth()] + " " + 
		date.getDate() + ". " + 
		date.getHours() + " : " + 
		date.getMinutes() + " : " + 
		seconds
	);

	setTimeout(function(){ setCTime() }, 1000);
	return;   
}


/* ======================= Hang man ========================= */
// constants
var POSSIBLE_WORDS = [
	"obdurate", "verisimilitude", "defenestrate",
	"obsequious", "dissonant", "today", "idempotent"
];
var MAX_GUESSES = 6;

// global variables
var guesses,
	guessCount,
	word;

function newGame() {
	var index;

	guessCount = MAX_GUESSES;
	guesses = "";

	index = Math.floor(Math.random() * POSSIBLE_WORDS.length);
	word = POSSIBLE_WORDS[index];
	document.getElementById("guessbutton").disabled = false;
	updatePage();
	return;
}

function updatePage() {
	var clueStr = "",
		clueElem = document.getElementById("clue"),
		guessStrElem = document.getElementById("guessstr"),
		imgElem = document.getElementById("hangmanpic"),
		srcInArr;

	for (var i=0; i < word.length; i++) {
		if (guesses.indexOf(word.charAt(i)) > -1) {
			clueStr += " " + word.charAt(i) + " ";
		} else {
			clueStr += " _ ";
		}
	}

	clueElem.innerHTML = clueStr;
	guessStrElem.innerHTML = guesses;

	srcInArr = imgElem.src.split('/');
	srcInArr = srcInArr.slice(0, srcInArr.length - 1);
	srcInArr.push("hangman" + guessCount + ".gif");
	imgElem.src = srcInArr.join('/');

	if (guessCount == 0) {
		guessStrElem.innerHTML = "You lose";
		return;
	} else if (clueStr.indexOf("_") == -1) {
		guessStrElem.innerHTML = "You Win";
		return;
	}
	return;
}

function guessLetter() {
	var hguessElem = document.getElementById("hguess"),
		clueElem = document.getElementById("clue"),
		letter = hguessElem.value;	
	// no reaction when already guessed letter
	if (guesses.indexOf(letter) > -1) return;
	// no reaction when already lose or win
	if (guessCount == 0) {
		return;
	} else if (clueElem.innerHTML.indexOf("_") == -1) {
		return;
	}

	// else
	guesses += letter;
	if (word.indexOf(letter) == -1) {
		guessCount--;
	}
	updatePage();

	// extra code
	hguessElem.value = '';
	hguessElem.focus();
	return;
}

function changeImage() {
	var imgElem = document.getElementById("picture");
	var imgList = [ "goat", "neoguri" ];

	for (var i=0; i < imgList.length; i++) {
		if (imgElem.src.indexOf(imgList[i]) > -1) {
			i = (i == imgList.length-1) ? 0 : i+1;
			imgElem.src = './imgs/' + imgList[i] + '.jpg';
			break;
		}
	}
	return;
}

var INTERVAL_ID;

function changeColor() {
	INTERVAL_ID = setInterval(flash, 1000);
	return;
}

function flash() {
	
	var target = document.getElementById("target");
	var colors = [
		{ bg: "yellow", txt: "blue" },
		{ bg: "green", txt: "red" }
	];
	var index = 0;

	if (target.style.backgroundColor == colors[0].bg) {
		index = 1;
	}

	target.style.backgroundColor = colors[index].bg;
	target.style.color = colors[index].txt;
	return;
}

function stopTextColor() {
	clearInterval(INTERVAL_ID);
	return;
}

function myMove() {
	var container = document.getElementById("container");
	var movingBox = document.getElementById("animate");
	var loc = 0;
	var limit = parseInt(window.getComputedStyle(container).height) - parseInt(window.getComputedStyle(movingBox).height);
 	var MOVE_INTERVAL;

 	var move = function() {
 		if (loc < limit) {
			loc++;
 			movingBox.style.top = loc + "px";
 			movingBox.style.left = loc + "px";
 		} else {
			clearInterval(MOVE_INTERVAL);
 		}
 	}
 	MOVE_INTERVAL = setInterval(move, 5);
}

window.onload = function() {
	setCTime();
	changeColor();
	document.getElementById("calcBtn").onclick = calc;
	document.getElementById("guessBtn").onclick = guess;
	document.getElementById("replayBtn").onclick = replay;
	document.getElementById("changeImgBtn").onclick = changeImage;
	document.getElementById("stopTextColorBtn").onclick = stopTextColor;
	document.getElementById("moveBtn").onclick = myMove;
	document.getElementById("guessbutton").onclick = guessLetter;
	document.getElementById("newgame").onclick = newGame;
	document.getElementById("opengoogle").onclick = function() {
		maxopen('http://www.google.com', 'resize=1, scrollbars=1, status=1');
	}
}
